const coin = {
    state: 0,
    flip: function() {
        let random = Math.floor(Math.random() * 2);
        this.state = random
    },
    toString: function() {
        if (this.state == 0){
            return "Heads"
        } else {
            return "Tails"
        }
    },
    toHTML: function() {
        const image = document.createElement('img')
        if (this.state === 0){
            image.src = 'images/HEADS.png'
        } else {
            image.src = 'images/TAILS.png'
        }
        return image
    }
 }

 
 function display20Flips() {
    const results = []
    for (let count = 0; count < 20; count++){
        coin.flip()
        let face = coin.toString()
        let res = document.createElement('p')
        res.textContent = face
        document.body.appendChild(res)
        results.push(face)
    }
    const ShowResult = document.createElement('div')
    ShowResult.textContent = `${results}`
    document.body.appendChild(ShowResult)
 }


 function display20Images() {
    const results = []
    for (let count = 0; count < 20; count++){
        coin.flip()
        let img = coin.toHTML()
        document.body.appendChild(img)
        results.push(coin.toString())
    }
    const ShowResult = document.createElement('div')
    ShowResult.textContent = `${results}`
    document.body.appendChild(ShowResult)
 }


 display20Flips()
 display20Images()